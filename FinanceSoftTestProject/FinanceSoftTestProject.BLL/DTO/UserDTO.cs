﻿namespace FinanceSoftTestProject.BLL.DTO
{
    /// <summary>
    /// Через данный класс мы передаем информацию о пользователях на уровень представления или наоборот будем получать данные с него
    /// </summary>
    public class UserDTO
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Role { get; set; }
    }
}
