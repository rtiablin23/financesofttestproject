﻿using System;

namespace FinanceSoftTestProject.BLL.DTO
{
    /// <summary>
    /// Через данный класс мы передаем информацию о cообщениях на уровень представления или наоборот будет получать данные с него
    /// </summary>
    public class MessageDTO
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Message { get; set; }
        public string UserName { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }
}
