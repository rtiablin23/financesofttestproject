﻿using FinanceSoftTestProject.BLL.DTO;
using FinanceSoftTestProject.BLL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FinanceSoftTestProject.BLL.Interfaces
{
    /// <summary>
    /// Через объекты данного интерфейса уровень представления будет взаимодействовать с уровнем доступа к данным
    /// </summary>
    public interface IUserService : IDisposable
    {
        Task<OperationDetails> Create(UserDTO userDto);
        Task<ClaimsIdentity> Authenticate(UserDTO userDto);
        Task SetInitialData(UserDTO adminDto, List<string> roles);
    }
}
