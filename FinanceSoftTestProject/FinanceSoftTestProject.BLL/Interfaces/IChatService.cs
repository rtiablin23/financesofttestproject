﻿using FinanceSoftTestProject.BLL.DTO;
using System;
using System.Collections.Generic;

namespace FinanceSoftTestProject.BLL.Interfaces
{
    /// <summary>
    /// Через объекты данного интерфейса уровень представления будет взаимодействовать с уровнем доступа к данным
    /// </summary>
    public interface IChatService : IDisposable
    {
        List<MessageDTO> GetMessages(DateTime? DateFrom = null, DateTime? DateTo = null);
        bool SendMessage(MessageDTO message);
    }
}
