﻿namespace FinanceSoftTestProject.BLL.Interfaces
{
    /// <summary>
    /// Фабрика сервисов для пользователя и чата
    /// </summary>
    public interface IServiceCreator
    {
        IUserService CreateUserService();
        IChatService CreateChatService();
    }
}
