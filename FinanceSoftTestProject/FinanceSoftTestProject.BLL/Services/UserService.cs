﻿using FinanceSoftTestProject.BLL.DTO;
using FinanceSoftTestProject.BLL.Infrastructure;
using FinanceSoftTestProject.BLL.Interfaces;
using FinanceSoftTestProject.DAL.Entities;
using FinanceSoftTestProject.DAL.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FinanceSoftTestProject.BLL.Services
{
    public class UserService : IUserService
    {
        // Добавляем свойство интерфейса IUnitOfWork для работы с контекстом данных
        IUnitOfWork Database { get; set; }

        public UserService(IUnitOfWork uow)
        {
            Database = uow;
        }

        // Создание пользователя
        public async Task<OperationDetails> Create(UserDTO userDto)
        {
            ApplicationUser user = await Database.UserManager.FindByEmailAsync(userDto.Email);
            if (user == null)
            {
                user = new ApplicationUser { Email = userDto.Email, UserName = userDto.Email };
                var result = await Database.UserManager.CreateAsync(user, userDto.Password);
                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");

                // Добавляем роль
                await Database.UserManager.AddToRoleAsync(user.Id, userDto.Role);

                // Создаем профиль клиента
                UserInfo UserInfo = new UserInfo { Id = user.Id, FirstName = userDto.FirstName, LastName = userDto.LastName };
                Database.ClientManager.Create(UserInfo);
                await Database.SaveAsync();
                return new OperationDetails(true, "Регистрация успешно пройдена", "");
            }
            else
            {
                return new OperationDetails(false, "Пользователь с таким логином уже существует", "Email");
            }
        }

        public async Task<ClaimsIdentity> Authenticate(UserDTO userDto)
        {
            ClaimsIdentity claim = null;
            // Находим пользователя
            ApplicationUser user = await Database.UserManager.FindAsync(userDto.Email, userDto.Password);
            // Авторизуем его и возвращаем объект ClaimsIdentity
            if (user != null)
            {
                claim = await Database.UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
            }
            return claim;
        }
        
        // Начальная инициализация бд
        public async Task SetInitialData(UserDTO adminDto, List<string> roles)
        {
            foreach (string roleName in roles)
            {
                try
                {
                    var role = await Database.RoleManager.FindByNameAsync(roleName);
                    if (role == null)
                    {
                        role = new ApplicationRole { Name = roleName };
                        await Database.RoleManager.CreateAsync(role);
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            await Create(adminDto);
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
