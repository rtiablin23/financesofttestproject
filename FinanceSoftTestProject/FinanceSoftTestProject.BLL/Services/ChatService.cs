﻿using FinanceSoftTestProject.BLL.DTO;
using FinanceSoftTestProject.BLL.Interfaces;
using FinanceSoftTestProject.DAL.Entities;
using FinanceSoftTestProject.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FinanceSoftTestProject.BLL.Services
{
    public class ChatService : IChatService
    {
        IUnitOfWork Database { get; set; }
        private readonly IRepository<ChatMessage> _chatRepository;

        public ChatService(IUnitOfWork uow)
        {
            Database = uow;
            _chatRepository = Database.GetRepository<ChatMessage>();
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        public List<MessageDTO> GetMessages(DateTime? DateFrom = null, DateTime? DateTo = null)
        {
            Func<ChatMessage, bool> predicate = x => true;
            Expression<Func<ChatMessage, object>> includeUser = x => x.User;

            if(DateFrom.HasValue && DateTo.HasValue)
            {
                predicate = x => x.CreatedDateTime.Date >= DateFrom.Value.Date && x.CreatedDateTime.Date <= DateTo.Value.Date;
            }
            else if (DateFrom.HasValue)
            {
                predicate = x => x.CreatedDateTime.Date >= DateFrom.Value.Date;
            }
            else if (DateTo.HasValue)
            {
                predicate = x => x.CreatedDateTime.Date <= DateTo.Value.Date;
            }

            var messages = _chatRepository.Get(predicate, includeUser);
            return messages.Select(x => new MessageDTO 
            {
                Id = x.Id,
                CreatedDateTime = x.CreatedDateTime,
                Message = x.Message,
                UserId = x.UserId,
                UserName = x.User.Email
            }).ToList();
        }

        public bool SendMessage(MessageDTO message)
        {
            if(message == null || message.Message.Length == 0)
            {
                return false;
            }

            var result = _chatRepository.Create(new ChatMessage
            {
                UserId = message.UserId,
                Message = message.Message,
                CreatedDateTime = DateTime.Now
            });

            return result > 0;
        }
    }
}
