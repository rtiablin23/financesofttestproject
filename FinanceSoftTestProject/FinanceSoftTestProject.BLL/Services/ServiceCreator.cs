﻿using FinanceSoftTestProject.BLL.Interfaces;
using FinanceSoftTestProject.DAL.Repositories;

namespace FinanceSoftTestProject.BLL.Services
{
    /// <summary>
    /// Реализация интерфейса IServiceCreator
    /// </summary>
    public class ServiceCreator : IServiceCreator
    {
        public IChatService CreateChatService()
        {
            return new ChatService(new UnitOfWork());
        }

        public IUserService CreateUserService()
        {
            return new UserService(new UnitOfWork());
        }
    }
}
