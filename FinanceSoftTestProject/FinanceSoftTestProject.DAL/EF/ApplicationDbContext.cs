﻿using FinanceSoftTestProject.DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace FinanceSoftTestProject.DAL.EF
{
    /// <summary>
    /// Класс контекста данных приложения наследуется от Identity и уже имеет все свойства Users и Roles
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public ApplicationDbContext() : base("ApplicationDbContext") { }
        public DbSet<UserInfo> UserInfos { get; set; }
        public DbSet<ChatMessage> ChatMessages { get; set; }
    }
}
