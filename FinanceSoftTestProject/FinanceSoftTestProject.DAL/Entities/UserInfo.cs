﻿using FinanceSoftTestProject.DAL.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FinanceSoftTestProject.DAL.Entities
{
    /// <summary>
    /// Модель отвечающая за пользователя и его свойства
    /// </summary>
    public class UserInfo : IEntity<int>
    {
        [Key]
        [ForeignKey("ApplicationUser")]
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
