﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace FinanceSoftTestProject.DAL.Entities
{
    public class ApplicationRole : IdentityRole<int, ApplicationUserRole>
    {
        public ApplicationRole() { }
    }
}