﻿using FinanceSoftTestProject.DAL.EF;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FinanceSoftTestProject.DAL.Entities
{
    public class ApplicationRoleStore : RoleStore<ApplicationRole, int, ApplicationUserRole>
    {
        public ApplicationRoleStore(ApplicationDbContext context) : base(context) { }
    }
}