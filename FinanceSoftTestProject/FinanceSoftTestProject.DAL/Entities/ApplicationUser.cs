﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace FinanceSoftTestProject.DAL.Entities
{
    public class ApplicationUser : IdentityUser<int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public virtual UserInfo UserInfo { get; set; }
    }
}
