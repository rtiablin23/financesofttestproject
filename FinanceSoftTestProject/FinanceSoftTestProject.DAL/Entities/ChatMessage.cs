﻿using FinanceSoftTestProject.DAL.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FinanceSoftTestProject.DAL.Entities
{
    /// <summary>
    /// Модель отвечающая за свойство сообщений в чате
    /// </summary>
    public class ChatMessage : IEntity<int>
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
