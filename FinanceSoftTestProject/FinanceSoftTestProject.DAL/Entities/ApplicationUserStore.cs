﻿using FinanceSoftTestProject.DAL.EF;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FinanceSoftTestProject.DAL.Entities
{
    public class ApplicationUserStore : UserStore<ApplicationUser, ApplicationRole, int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public ApplicationUserStore(ApplicationDbContext context) : base(context) { }
    }
}