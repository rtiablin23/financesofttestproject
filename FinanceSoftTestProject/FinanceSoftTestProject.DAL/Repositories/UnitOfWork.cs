﻿using FinanceSoftTestProject.DAL.EF;
using FinanceSoftTestProject.DAL.Entities;
using FinanceSoftTestProject.DAL.Identity;
using FinanceSoftTestProject.DAL.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace FinanceSoftTestProject.DAL.Repositories
{
    /// <summary>
    /// Класс инкапсулирует все менеджеры для работы с сущностями в виде свойств и хранит общий контекст данных
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly ApplicationUserManager _userManager;
        private readonly ApplicationRoleManager _roleManager;
        private readonly IClientManager _clientManager;
        private readonly ConcurrentDictionary<Type, object> _repositories;
        private bool disposed = false;

        public UnitOfWork()
        {
            _context = new ApplicationDbContext();
            _userManager = new ApplicationUserManager(new ApplicationUserStore(_context));
            _roleManager = new ApplicationRoleManager(new ApplicationRoleStore(_context));
            _clientManager = new ClientManager(_context);
            _repositories = new ConcurrentDictionary<Type, object>();
        }

        public ApplicationUserManager UserManager => _userManager;

        public ApplicationRoleManager RoleManager => _roleManager;

        public IClientManager ClientManager => _clientManager;

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity<int>
        {
            return _repositories.GetOrAdd(typeof(TEntity), new Repository<TEntity>(_context)) as IRepository<TEntity>;
        }
    }
}
