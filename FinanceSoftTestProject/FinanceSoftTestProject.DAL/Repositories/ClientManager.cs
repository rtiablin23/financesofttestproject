﻿using FinanceSoftTestProject.DAL.EF;
using FinanceSoftTestProject.DAL.Entities;
using FinanceSoftTestProject.DAL.Interfaces;

namespace FinanceSoftTestProject.DAL.Repositories
{
    /// <summary>
    /// Класс управления профилями, реализует интерфейс IClientManager
    /// </summary>
    public class ClientManager : IClientManager
    {   
        public ApplicationDbContext Database { get; set; }
        public ClientManager(ApplicationDbContext db)
        {
            Database = db;
        }

        public void Create(UserInfo item)
        {
            Database.UserInfos.Add(item);
            Database.SaveChanges();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
