﻿namespace FinanceSoftTestProject.DAL.Interfaces
{
    /// <summary>
    /// Интерфейс объектов с идентификатором
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}
