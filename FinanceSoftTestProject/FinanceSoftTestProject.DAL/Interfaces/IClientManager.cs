﻿using FinanceSoftTestProject.DAL.Entities;
using System;

namespace FinanceSoftTestProject.DAL.Interfaces
{
    /// <summary>
    /// Данный интерфейс содержит один метод для создания нового профиля пользователя
    /// </summary>
    public interface IClientManager : IDisposable
    {
        void Create(UserInfo item);
    }
}
