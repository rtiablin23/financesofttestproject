﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace FinanceSoftTestProject.DAL.Interfaces
{
    /// <summary>
    /// Репозиторий позволяет абстрагироваться от конкретных подключений к источникам данных,
    /// и является промежуточным звеном между классами, непосредственно взаимодействующими с данными, и остальной программой
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> where TEntity : class, IEntity<int>
    {
        int Create(TEntity item);
        TEntity FindById(int id);
        IEnumerable<TEntity> Get();
        IEnumerable<TEntity> Get(Func<TEntity, bool> predicate, params Expression<Func<TEntity, object>>[] includes);
        void Remove(TEntity item);
        void Update(TEntity item);
    }
}
