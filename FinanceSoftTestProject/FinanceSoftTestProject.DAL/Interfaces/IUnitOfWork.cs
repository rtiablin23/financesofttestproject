﻿using FinanceSoftTestProject.DAL.Identity;
using System;
using System.Threading.Tasks;

namespace FinanceSoftTestProject.DAL.Interfaces
{
    /// <summary>
    /// Объект UnitOfWork содержит ссылки на менеджеры пользователей и ролей, а также на репозиторий пользователей
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        ApplicationRoleManager RoleManager { get; }
        IClientManager ClientManager { get; }
        Task SaveAsync();
        IRepository<IEntity> GetRepository<IEntity>() where IEntity : class, IEntity<int>;
    }
}
