﻿using FinanceSoftTestProject.DAL.Entities;
using Microsoft.AspNet.Identity;

namespace FinanceSoftTestProject.DAL.Identity
{
    /// <summary>
    /// Данный класс будет управлять ролями
    /// </summary>
    public class ApplicationRoleManager : RoleManager<ApplicationRole, int>
    {
        public ApplicationRoleManager(ApplicationRoleStore store) : base(store) { }
    }
}
