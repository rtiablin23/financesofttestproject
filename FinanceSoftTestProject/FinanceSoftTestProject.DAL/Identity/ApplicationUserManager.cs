﻿using FinanceSoftTestProject.DAL.Entities;
using Microsoft.AspNet.Identity;
using System.Collections.ObjectModel;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FinanceSoftTestProject.DAL.Identity
{
    /// <summary>
    /// Данный класс будет управлять пользователями: добавлять их в базу данных и аутентифицировать
    /// </summary>
    public class ApplicationUserManager : UserManager<ApplicationUser, int>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser, int> store) : base(store) { }
    }
}
