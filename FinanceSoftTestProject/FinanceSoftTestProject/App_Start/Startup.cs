﻿using FinanceSoftTestProject.BLL.Interfaces;
using FinanceSoftTestProject.BLL.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Web.Routing;

[assembly: OwinStartup(typeof(FinanceSoftTestProject.App_Start.Startup))]

namespace FinanceSoftTestProject.App_Start
{
    /// <summary>
    /// Класс Startup является входой точкой в приложении. Класс производит конфигурацию приложения, настраивает сервисы, которые приложение будет использовать
    /// </summary>
    public class Startup
    {
        private readonly IServiceCreator serviceCreator = new ServiceCreator();
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext(CreateUserService);
            app.CreatePerOwinContext(CreateChatService);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });

            // Задействует функционал SignalR
            app.MapSignalR();

        }

        private IUserService CreateUserService()
        {
            return serviceCreator.CreateUserService();
        }

        private IChatService CreateChatService()
        {
            return serviceCreator.CreateChatService();
        }
    }
}