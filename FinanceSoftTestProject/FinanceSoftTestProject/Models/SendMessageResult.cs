﻿using FinanceSoftTestProject.BLL.DTO;

namespace FinanceSoftTestProject.Models
{
    public class SendMessageResult : ResultModel
    {
        public MessageDTO MessageDTO {get; set; }
    }
}