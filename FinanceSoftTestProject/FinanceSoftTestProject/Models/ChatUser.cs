﻿namespace FinanceSoftTestProject.Models
{
    public class ChatUser
    {
        public int UserId { get; set; }
        public string ConnectionId { get; set; }
        public string Name { get; set; }
    }
}