﻿namespace FinanceSoftTestProject.Models
{
    public class ResultModel
    {
        public bool IsSucceed { get; set; }
        public string ErrorMessages { get; set; }
    }
}