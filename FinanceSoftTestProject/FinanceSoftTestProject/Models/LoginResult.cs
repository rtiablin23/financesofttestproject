﻿using FinanceSoftTestProject.BLL.DTO;

namespace FinanceSoftTestProject.Models
{
    public class LoginResult : ResultModel
    {
        public UserDTO UserData {get; set; }
    }
}