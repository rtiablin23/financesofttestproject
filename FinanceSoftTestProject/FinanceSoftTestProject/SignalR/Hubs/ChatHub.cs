﻿using FinanceSoftTestProject.Models;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinanceSoftTestProject.SignalR.Hubs
{
    public class ChatHub : Hub
    {
        static readonly List<ChatUser> Users = new List<ChatUser>();

        // Отправка сообщений
        public void Send(string name, string message, DateTime createdDateTime)
        {
            Clients.All.addMessage(name, message, createdDateTime.ToString());
        }

        // Подключение пользователя
        public void Connect(string userName, int userId)
        {
            var id = Context.ConnectionId;
            string name = Context.User.Identity.Name;
            var user = Users.FirstOrDefault(x => x.Name == name);
            if (!Users.Any(x => x.ConnectionId == id))
            {
                Users.Add(new ChatUser { ConnectionId = id, Name = userName, UserId = userId });

                // Посылаем сообщение текущему пользователю
                Clients.Caller.onConnected(id, userName, Users);

                // Посылаем сообщение всем пользователям, кроме текущего
                Clients.AllExcept(id).onNewUserConnected(id, userName);
            }
        }

        // Получение информации о пользователе для вывода в чат сообщений
        public void GetUserInfo(int userId, string userName)
        {
            var user = Users.FirstOrDefault(x => x.UserId == userId);
            if (user != null)
            {
                Clients.Caller.onConnected(user.ConnectionId, user.Name, Users);
                Clients.AllExcept(user.ConnectionId).onNewUserConnected(user.ConnectionId, user.Name);
            }
            else
            {
                Connect(userName, userId);
            }
        }

        // Отключение пользователя из чата
        public override Task OnDisconnected(bool stopCalled)
        {
            var item = Users.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                Users.Remove(item);
                var id = Context.ConnectionId;
                Clients.All.onUserDisconnected(id, item.Name);
            }
            return base.OnDisconnected(stopCalled);
        }
    }
}