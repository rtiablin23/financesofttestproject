﻿using FinanceSoftTestProject.BLL.DTO;
using FinanceSoftTestProject.BLL.Interfaces;
using FinanceSoftTestProject.Models;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinanceSoftTestProject.Controllers
{
    public class ChatController : Controller
    {
        private IChatService ChatService => HttpContext.GetOwinContext().Get<IChatService>();

        // GET: Chat
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        // Отображение сообщений в чате
        public ActionResult ChatMessages(DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var messages = ChatService.GetMessages(dateFrom, dateTo);
            return PartialView(messages.OrderByDescending(x => x.CreatedDateTime));
        }

        [HttpPost]
        public ActionResult Send(MessageDTO message)
        {
            if(message == null)
            {
                return Json(new SendMessageResult { IsSucceed = false, ErrorMessages = "Message can't be null" }, JsonRequestBehavior.AllowGet);
            }
            var result = ChatService.SendMessage(message);
            return Json(new SendMessageResult { IsSucceed = result, MessageDTO = message }, JsonRequestBehavior.AllowGet);
        }
    }
}