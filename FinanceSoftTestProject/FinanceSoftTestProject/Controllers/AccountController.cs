﻿using FinanceSoftTestProject.BLL.DTO;
using FinanceSoftTestProject.BLL.Infrastructure;
using FinanceSoftTestProject.BLL.Interfaces;
using FinanceSoftTestProject.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FinanceSoftTestProject.Controllers
{
    public class AccountController : Controller
    {
        private IUserService UserService => HttpContext.GetOwinContext().GetUserManager<IUserService>();
        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model)
        {
            await SetInitialDataAsync();
            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO { Email = model.Email, Password = model.Password };
                ClaimsIdentity claim = await UserService.Authenticate(userDto);
                if (claim == null)
                {
                    ModelState.AddModelError("", "Неверный логин или пароль.");
                }
                else
                {
                    if (int.TryParse(claim.GetUserId(), out int userId))
                    {
                        userDto.Id = userId;
                    }

                    SignIn(claim);
                    return Json(new LoginResult { IsSucceed = true, UserData = userDto}, JsonRequestBehavior.AllowGet);
                }
            }

            var errorsList = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return Json(new LoginResult { IsSucceed = false, ErrorMessages = string.Join("; ", errorsList) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            await SetInitialDataAsync();
            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO
                {
                    Email = model.Email,
                    Password = model.Password,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Role = "user"
                };
                OperationDetails operationDetails = await UserService.Create(userDto);
                if (operationDetails.Succedeed)
                {
                    ClaimsIdentity claim = await UserService.Authenticate(userDto);
                    var principal = new ClaimsPrincipal(claim);

                    if (int.TryParse(claim.GetUserId(), out int userId))
                    {
                        userDto.Id = userId;
                    }

                    SignIn(claim);
                    return Json(new LoginResult { IsSucceed = true, UserData = userDto}, JsonRequestBehavior.AllowGet);
                }
                else
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
            }

            var errorsList = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return Json(new LoginResult { IsSucceed = false, ErrorMessages = string.Join("; ", errorsList)}, JsonRequestBehavior.AllowGet);
        }

        private void SignIn(ClaimsIdentity claim)
        {
            AuthenticationManager.SignOut();
            AuthenticationManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = true
            }, claim);
        }

        private async Task SetInitialDataAsync()
        {
            await UserService.SetInitialData(new UserDTO
            {
                Email = "husketbbx@gmail.ru",
                UserName = "husketbbx@gmail.ru",
                Password = "123123",
                FirstName = "Roman",
                LastName = "Sexov",
                Role = "admin",
            }, new List<string> { "user", "admin" });
        }
    }
}