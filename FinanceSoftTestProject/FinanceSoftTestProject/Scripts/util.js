﻿$(function () {
    // Ссылка на автоматически-сгенерированный прокси хаба
    var chat = $.connection.chatHub;

    // ViewModel knockoutjs c и использовнием Jquery и AJAX запросов
    var chatViewModel = function () {
        var model = this;
        model.txtUserId = $('.main').attr('data-userId');
        model.txtUserName = $('.main').attr('data-userName');
        model.txtMessage = ko.observable('');
        model.dateFrom = ko.observable("");
        model.dateTo = ko.observable("");

        filterDate = function () {
            $.post("/Chat/ChatMessages", { dateFrom: model.dateFrom(), dateTo: model.dateTo() }, function (result) {
                $('#chatroom').html(result)
            });
        };

        $('#TxtMessage').on('keypress', function (e) {
            if (e.which == 13) {
                sendMessage();
            }
        });

        sendMessage = function () {
            if (model.txtMessage().trim() == "") {
                alert("Введите сообщение")
            }
            else {
                let url = '/Chat/Send'
                $.ajax({
                    url: url,
                    data: {
                        "message": {
                            "UserId": model.txtUserId,
                            "Message": model.txtMessage(),
                            "UserName": model.txtUserName,
                            "CreatedDateTime": new Date().toISOString()
                        }
                    },
                    type: 'POST',
                    success: function (result) {
                        if (result.IsSucceed) {
                            $('#loader').addClass('hidden')
                            let date = GetDate(result.MessageDTO.CreatedDateTime);
                            chat.server.send(result.MessageDTO.UserName, result.MessageDTO.Message, date)
                        } else {
                            $('#loader').addClass('hidden')
                        }
                    },
                    error: function (response) {
                        console.log(response.responseText);
                    }
                });
                model.txtMessage('');
            }
        };
    }

    var vm = new chatViewModel();
    // Запуск работы ViewModel на Knockout
    ko.applyBindings(vm);

   
    // Объявление функции, которая хаб вызывает при получении сообщений
    chat.client.addMessage = function (name, message, createdDateTime) {
        // Добавление сообщений на веб-страницу 
        $('#chatroom').prepend('<p>' + htmlEncode(createdDateTime) + ' <b>' + htmlEncode(name)
            + '</b>: </p><p class="chatMessage">' + htmlEncode(message) + '</p>');
    };

    // Функция, вызываемая при подключении нового пользователя
    chat.client.onConnected = function (id, userName, allUsers) {
        $('#chatBody').show();
        // установка в скрытых полях имени и id текущего пользователя
        $('#hdId').val(id);
        $('#username').val(userName);
        $('#header').html('<h3>Добро пожаловать, ' + userName + '</h3>');

        // Добавление всех пользователей
        for (i = 0; i < allUsers.length; i++) {

            AddUser(allUsers[i].ConnectionId, allUsers[i].Name);
        }
    }

    //Добовляем нового пользователя на странице
    chat.client.onNewUserConnected = function (id, userName) {
        AddUser(id, userName);
    }

    // Удаляем пользователя
    chat.client.onUserDisconnected = function (id, userName) {

        $('#' + id).remove();
    }


    // Открываем соединение
    $.connection.hub.start().done(function () {
        // Вход пользователя 
        $("#loginForm").submit(function (e) {
            e.preventDefault();
            $('#loader').removeClass('hidden')
            let url = '/Account/Login'
            $.ajax({
                url: url,
                data: $(this).serializeArray(),
                type: 'POST',
                success: function (result) {
                    if (result.IsSucceed) {
                        $('#loader').addClass('hidden')
                        chat.server.connect(result.UserData.Email, result.UserData.Id);
                        window.location.href = "/Chat/Index";
                    } else {
                        $('#validationErrors').html(`<p class = "text-danger">${result.ErrorMessages}</p>`)
                        $('#loader').addClass('hidden')
                    }
                },
                error: function (response) {
                    console.log(response.responseText);
                }
            })
        });

        

        // Выход из чата
        $('#logoutForm').submit(function (e) {
            let userId = $('#logoutForm .logoff').attr('data-userId');
            chat.server.logoff(false, userId);
        });

        // Подключает пользователя в чат
        $(document).ready(function (e) {
            let url = window.location.pathname.toLowerCase();
            if (url === '/chat' || url === '/chat/index') {
                let loadedBefore = $('.main').attr('data-isLoadedBefore');
                if (loadedBefore.toLowerCase() === 'true') {
                    return;
                }
                let userId = $('.main').attr('data-userId');
                let userName = $('.main').attr('data-userName');
                chat.server.getUserInfo(userId, userName)

                $('.main').attr('data-isLoadedBefore', 'true');
                UpdateChatMessages();
            }
        });

        $("#registerForm").submit(function (e) {
            e.preventDefault();
            $('#loader').removeClass('hidden')
            let url = '/Account/Register'
            $.ajax({
                url: url,
                data: $(this).serializeArray(),
                type: 'POST',
                success: function (result) {
                    if (result.IsSucceed) {
                        $('#loader').addClass('hidden')
                        chat.server.connect(result.UserData.Email, result.UserData.Id);
                        window.location.href = "/Home/Index";
                    } else {
                        $('#validationErrors').html(`<p class = "text-danger">${result.ErrorMessages}</p>`)
                        $('#loader').addClass('hidden')
                    }
                },
                error: function (response) {
                    console.log(response.responseText);
                }
            })
        });
    });
});

// Кодирование тегов пресекает возможные попытки вставок HTML кода JavaScript и прочих манипуляций
function htmlEncode(value) {
    var encodedValue = $('<div />').text(value).html();
    return encodedValue;
}

// Вывод пользователей онлайн 
function AddUser(id, name) {

    var userId = $('#userId').val();

    if (userId != id) {

        $("#chatusers").append('<p id="' + id + '"><b>' + name + '</b></p>');
    }
}

// Функция получения конвертации времени из формата JS в стандарт
function GetDate(date) {
    var options = { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric' };
    let newDate = new Date(Date(parseInt(date.substr(6))));
    return newDate.toLocaleDateString("en-US", options);
}

// Функция обновления чата
function UpdateChatMessages(dateFrom, dateTo) {
    if (dateFrom === undefined || dateFrom === '') {
        dateFrom = null;
    }

    if (dateTo === undefined || dateTo === '') {
        dateTo = null;
    }

    let url = `/Chat/ChatMessages?dateFrom=${dateFrom}&dateTo=${dateTo}`;
    $.ajax({
        url: url,
        type: 'GET',
        success: function (result) {
            $('#chatroom').html(result)
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

